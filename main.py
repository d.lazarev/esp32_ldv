import os
import esp32_ldv

counter = 0
ap = esp32_ldv.scan_stations()

for i in ap:
    print('WiFi AC: %s, channel: %s, RSSI: %s' % tuple(i))
    counter += 1

print("\nTotal {} WiFi points available\n\n".format(counter))
try:
    os.stat('/etc')
except FileNotFoundError:
    os.mkdir('/etc')

f_know_ap = open('/etc/know_access_points', 'w+')
know_ap = []
for line in f_know_ap:
    know_ap.append(line)

k_ap = esp32_ldv.know_ap_search(ap, know_ap)
if not k_ap:
    esp32_ldv.create_config_ap(ap)
