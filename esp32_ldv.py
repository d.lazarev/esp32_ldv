import network
from machine import Pin, Timer
from microWebSrv import MicroWebSrv


ap_list = []


def _httpHandlerApPost(httpClient, httpResponse):
    ap = httpClient.ReadRequestPostedFormData()
    content = """\
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8 />
            <title>Found access points</title>
        </head>
        <body>
            <h1>Found access points</h1>
            %s<br />
        </body>
    </html>
    """ % (MicroWebSrv.HTMLEscape(ap[0]))
    httpResponse.WriteResponseOk(headers=None, contentType="text/html", contentCharset="UTF-8", content=content)


route_handlers = [
    ("/ap", "POST", _httpHandlerApPost)
]


server_config = MicroWebSrv(webPath="/www", routeHandlers=route_handlers)


def _acceptWebSocketCallback(webSocket, httpClient) :
    print("WS ACCEPT")
    webSocket.RecvTextCallback   = _recvTextCallback
    webSocket.RecvBinaryCallback = _recvBinaryCallback
    webSocket.ClosedCallback     = _closedCallback

def _recvTextCallback(webSocket, msg) :
    print("WS RECV TEXT : %s" % msg)
    webSocket.SendText("Reply for %s" % msg)

def _recvBinaryCallback(webSocket, data) :
    print("WS RECV DATA : %s" % data)

def _closedCallback(webSocket) :
    print("WS CLOSED")


def know_ap_search(ap, know_ap_list):
    for i in ap:
        result = [e for e in know_ap_list if e[0] == i[0]]
        if result:
            return result
    return False


def create_config_ap(ap):
    global ap_list
    global server_config
    ap_me = network.WLAN(network.AP_IF)
    ap_me.active(True)
    ap_me.config(essid='ESPLDV')
    p2 = Pin(2, Pin.OUT)
    ap_list.extend('<li>' + e[0] + '</li>' for e in ap)
    server_config.WebSocketThreaded = False
    print("Starting config web server...")
    tim0 = Timer(0)
    tim0.init(period=350, mode=Timer.PERIODIC, callback=lambda t: p2.value(not p2.value()))
    server_config.AcceptWebSocketCallback = _acceptWebSocketCallback
    server_config.Start()
    return ap_me


def scan_stations():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    ap = [(i[0].decode(), i[2], i[3]) for i in wlan.scan()]
    return ap
